fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json) => console.log(json));

/*fetch("https://jsonplaceholder.typicode.com/todos?userId=1")
.then((response)=>response.json())
.then((json)=>console.log(json));*/

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json.title));


fetch('https://jsonplaceholder.typicode.com/todos?userId=1&id=1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		userId: 100,
		title: "Make Activity",
		body:"False"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "New Todo"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/2",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title: "Updated Todo",
		description: "Update todo item",
		status: "true",
		dateCompleted: "4/21/2022",
		userId: 15
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

